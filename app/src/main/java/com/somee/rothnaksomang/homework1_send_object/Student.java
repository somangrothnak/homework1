package com.somee.rothnaksomang.homework1_send_object;

import android.os.Parcel;
import android.os.Parcelable;

public class Student implements Parcelable {

    private String name;
    private String phoneNunmber;
    private String className;

    protected Student(Parcel in) {
        name = in.readString();
        phoneNunmber = in.readString();
        className = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(phoneNunmber);
        dest.writeString(className);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNunmber() {
        return phoneNunmber;
    }

    public void setPhoneNunmber(String phoneNunmber) {
        this.phoneNunmber = phoneNunmber;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Student(String name, String phoneNunmber, String className) {
        this.name = name;
        this.phoneNunmber = phoneNunmber;
        this.className = className;
    }

}
