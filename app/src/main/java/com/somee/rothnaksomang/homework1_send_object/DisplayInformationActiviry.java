package com.somee.rothnaksomang.homework1_send_object;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DisplayInformationActiviry extends AppCompatActivity {

//    declare variable
    Button btnback;
    TextView tvName_Display,tvPhoneNumber_Display,tvClassName_Display;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_displayinformation);

//        create a referent from view in java code to view in xml code
        tvName_Display=findViewById(R.id.tvName);
        tvPhoneNumber_Display=findViewById(R.id.tvPhone);
        tvClassName_Display=findViewById(R.id.tvClass);

//        get Object from Main Activity
        Intent i=getIntent();
        Student s=i.getParcelableExtra("message");

//        Declare variable and set Value to variable from object
        String name=s.getName();
        String phoneNumber=s.getPhoneNunmber();
        String className=s.getClassName();

//        set Value to TextView
        tvName_Display.setText(name);
        tvPhoneNumber_Display.setText(phoneNumber);
        tvClassName_Display.setText(className);

//        Call Function for Button Back
        startMainActivity(s);
    }

    public void startMainActivity(final Student student){

//        create a referent from view in java code to view in xml code
        btnback=findViewById(R.id.btnBack);

        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                send Object back to Main Activity
                Intent i=new Intent(getApplicationContext(),MainActivity.class);
                i.putExtra("message",student);
                startActivity(i);
            }
        });
    }


}
