package com.somee.rothnaksomang.homework1_send_object;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

//    declare variable
    Button btnRegister,btnPhoneCall;
    EditText etName_Main,etPhoneNumber_Main,etClassName_Main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        create a referent from view in java code to view in xml code
        etName_Main=findViewById(R.id.etName);
        etPhoneNumber_Main=findViewById(R.id.etPhone);
        etClassName_Main=findViewById(R.id.etClass);

//        declare a object from Intent
        Intent i=getIntent();
//        declare a object of Student for getting a object from Display Activity
//        inialize null value to object
        Student s=null;
//        set object of Student equals object from Display Activity
         s=i.getParcelableExtra("message");

//         Check object get value from Display Activity or not
        if(s != null){
//            declare variable for get data from object
            String name=s.getName();
            String phoneNumber=s.getPhoneNunmber();
            String className=s.getClassName();

//            set value to EditText
            etName_Main.setText(name);
            etPhoneNumber_Main.setText(phoneNumber);
            etClassName_Main.setText(className);
        }
//        Call Function for Button Register
        startDisplayActivity();
//        Call Function for Button Phone Call
        startPhoneCall();
    }
    public void startDisplayActivity(){

//        create a referent from view in java code to view in xml code
        etName_Main=findViewById(R.id.etName);
        etPhoneNumber_Main=findViewById(R.id.etPhone);
        etClassName_Main=findViewById(R.id.etClass);
        btnRegister=findViewById(R.id.btnRegister);

//        set Event on Click to Button Register
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                declare variable for get data from View in Main Activity
                String name=etName_Main.getText().toString();
                String phoneNumber=etPhoneNumber_Main.getText().toString();
                String className=etClassName_Main.getText().toString();

//                check value on view
//                not null
                if(name.equals("")){
                    etName_Main.requestFocus();
//                    alert Message show User
                    Toast.makeText(getApplicationContext(),"required Name",Toast.LENGTH_SHORT).show();
                }else if(phoneNumber.equals("")){
                    etPhoneNumber_Main.requestFocus();
//                    alert Message show User
                    Toast.makeText(getApplicationContext(),"required Phone Number",Toast.LENGTH_SHORT).show();
                }else if(className.equals("")){
                    etClassName_Main.requestFocus();
//                    alert Message show User
                    Toast.makeText(getApplicationContext(),"required Class Name",Toast.LENGTH_LONG).show();
                }else{
//                    Send Data to DisplayInformationActivity by Explicit Intent
//                    create a Object from Intent for calling DisplayInformationActiviry
                    Intent i=new Intent(getApplicationContext(),DisplayInformationActiviry.class);

//                    set Value to Object Student
                    Student student=new Student(name,phoneNumber,className);
//                    put Data to Intent object
                    i.putExtra("message",student);
//                    Start Activity
                    startActivity(i);

//                    set EditText Null
                    etName_Main.setText("");
                    etPhoneNumber_Main.setText("");
                    etClassName_Main.setText("");
                }

            }
        });
    }
    public void startPhoneCall(){
//        create a referent from view in java code to view in xml code
        btnPhoneCall=findViewById(R.id.btnPhoneCall);

//        set Event on Click to Button Phone Call
        btnPhoneCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                set static value
                String phoneNumber="tel:098252329";

//                send data to other Dial App by Action ACTION_DIAL
//                user Implicit Intent
                Intent i=new Intent(Intent.ACTION_DIAL, Uri.parse(phoneNumber));
//                start Activity
                startActivity(i);
            }
        });
    }



}
